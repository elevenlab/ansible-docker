Ansible Role: docker
===================

Install and configure [Docker](https://www.docker.com/) Daemon on Debian servers. 


----------

Role variables
-------------

Available variables are listed below, along with default values (see `defaults/main.yml`):

    docker_version: 18.03
The Docker version this role will install `{{docker_version}}*`

    docker_bind_ip: 0.0.0.0
Default ip Docker daemon will bind when a port is published (it can be overridden by each service or container)

    docker_swarm_advertise_ip: not defined
The default ip the Docker daemon will advertise to other swarm node (it can be overriden during `swarm init` or `swarm join`)

    docker_log:
      driver: "json-file"
      opts:
        max-size: 20m
        max-file: 5
The docker log option to set for containers. Driver and related options can be use consistently with the [documentation](https://docs.docker.com/config/containers/logging/configure/#supported-logging-drivers).

Dependencies
-------------------
None.

Example Playbook
--------------------------
    - name: Install and configure Docker
      hosts: all
      roles:
        - role: docker
          docker_bind_ip: 10.0.0.8
          docker_swarm_advertise_ip: 192.168.0.111

TODO
---------
- Extend config, in particular dns section.